unit MGis.ControlVersion;

interface

uses
  Winapi.Windows, Winapi.Messages,
  System.SysUtils, System.Classes, System.IOUtils,
  Vcl.Graphics, Vcl.Controls, Vcl.SvcMgr, Vcl.Dialogs, IdBaseComponent,
  IdComponent, IdCustomTCPServer, IdCustomHTTPServer, IdHTTPServer, IdContext;

type
  TControl_Version = class(TService)
    IdHTTPServer1: TIdHTTPServer;
    procedure IdHTTPServer1CommandGet(AContext: TIdContext;
      ARequestInfo: TIdHTTPRequestInfo; AResponseInfo: TIdHTTPResponseInfo);
    procedure ServiceExecute(Sender: TService);
  private
    FActVersion:string;
    FBranch:string;
  public
    function GetServiceController: TServiceController; override;
    { Public declarations }
  end;

var
  Control_Version: TControl_Version;

implementation

{$R *.DFM}

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  Control_Version.Controller(CtrlCode);
end;

function TControl_Version.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

procedure TControl_Version.IdHTTPServer1CommandGet(AContext: TIdContext; ARequestInfo: TIdHTTPRequestInfo; AResponseInfo: TIdHTTPResponseInfo);
begin
  if Trim(ARequestInfo.Document)<>FBranch then
  begin
    AResponseInfo.ContentText:='';
    AResponseInfo.WriteContent;
    EXIT;
  end;
    AResponseInfo.ContentText:=FActVersion;
    AResponseInfo.WriteContent;
end;

procedure TControl_Version.ServiceExecute(Sender: TService);
var
  StrLst:TStringList;
begin
  StrLst:=TStringList.Create;
  TRY
    StrLst.LoadFromFile(TPath.GetDirectoryName(ParamStr(0))+TPath.DirectorySeparatorChar+'MGisSrvCtrlVersion.config');
    IdHTTPServer1.Bindings.Add.IP:=StrLst.Values['Address'];
    IdHTTPServer1.Bindings.Add.Port:=StrToInt(StrLst.Values['Port']);
    FActVersion:=StrLst.Values['AppVersion'];
    FBranch:=StrLst.Values['Branch'];
    TRY
      IdHTTPServer1.Active:=true;
    EXCEPT
      raise Exception.Create('������ ������� �������!');
    END;
  FINALLY
    StrLst.Free;
  END;
  While not Terminated do
    ServiceThread.ProcessRequests(true);
end;

end.
